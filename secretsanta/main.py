import os
import yaml
import urllib
import random
import http.client
import logging as lg
from typing import Dict

from models.User import User
import config
import phrases

SLACK_BASE_URL="slack.com"
SLACK_POST_CHAT="/api/chat.postMessage?token=%s&channel=%s&text=%s&username=%s"

participants = []

def main():
    # A few sanity checks first
    validate_config()

    # extracts users list
    participants   = get_users()

    # assigns everyone someone to give a gift to
    assigned_users = assign(participants)

    # gift time
    for gifter, giftee in assigned_users.items():
        gifterUser = participants[gifter]
        gifteeUser = participants[giftee]

        postMessages(gifterUser, gifteeUser)

    # Do secet Santa here

def assign(users: Dict):
    """
        Application of Numberphile's Secret Santa algorithm
    """
    # Makes first have of the cards
    gifters = [ u.username for k, u in users.items() ]

    # Shuffle it just for fun
    random.shuffle(gifters)

    # Puts the last item at the beginning of the list, now no one makes a gift to themselves for sure.
    giftees = [ gifters[-1] ] + gifters[:-1]

    return dict(zip(gifters, giftees))

def postMessages(gifter: User, giftee: User):
    """
        Sends message!
    """
    message = "Bonjour %s, tu offriras un cadeau à %s !" % (gifter.username, giftee.username)

    slack_client = http.client.HTTPSConnection(SLACK_BASE_URL)
    safe_message = urllib.parse.quote(message)
    safe_send_as = urllib.parse.quote(config.SLACK_SEND_AS)
    url          = SLACK_POST_CHAT % (config.SLACK_TOKEN, gifter.slack_id, safe_message, safe_send_as)

    print(url)

    slack_client.request("GET", url)

    response = slack_client.getresponse()

    print(response.status)

def get_users():
    """
        This is just for testing, we'll have a React app later
    """

    current_dir = os.path.dirname(os.path.realpath(__file__))
    file_path   = os.path.join(current_dir, 'users.yaml')

    with open(file_path, 'r') as file:
        users = yaml.load(file, Loader=yaml.BaseLoader)

    return { u['username']: User(u['username'], u['slack_id']) for u in users['users'] }

def validate_config():
    lang=''

    if not hasattr(config, 'LANG'):
        lg.warning("No language defined, falling back to EN.")
        lang='en'
    else:
        lang=config.LANG.lower()

    if lang not in phrases.PHRASES.keys():
        lg.error('Phrase not defined for language %s! Aborting.' % lang)
        exit()

if '__main__' == __name__:
    main()