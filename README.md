# PythonSecretSanta

## Why?

Here's why: https://www.youtube.com/watch?v=5kC5k5QBqcc

## Setup

Before running the script, you need a Slack API token (here: https://api.slack.com/custom-integrations/legacy-tokens).

Then, rename the `secretsanta/config.py.dist` file to `secretsanta/config.py` and add your configuration (Slack token and sender name).

Next, rename the `secretsanta/users.yaml.dist` file to `secretsanta/users.yaml` and add your friends / colleagues / people you want to make uncomfortable with the idea of finding a gift for a stranger.

*Note:* There are two examples users in the file :)

```bash
    $ virtualenv -p python3 env # 1st time only
    $ source env/bin/activate
    $ pip install -r requirements.txt # 1st time only
```

## Run the script

```bash
    $ source env/bin/activate # every time you want to run the script
    $ python secretsanta/main.py
```